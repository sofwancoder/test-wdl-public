version 1.0



# ---------
# Workflow
# ---------

workflow spammer_wdl {

    call task_A

}



# -------
# Task A
# -------

task task_A {
    command {
        echo "Hello World"
    }
    output {
        String out = read_string(stdout())
    }
    runtime {
        docker: "quay.io/lifebitai/ubuntu:18.10"
    }
}
